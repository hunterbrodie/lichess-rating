use std::env;

use serde_json::Value;

fn main()
{
	let args: Vec<String> = env::args().collect();

	if args.len() == 3
	{
		scrape(args[1].clone(), args[2].clone());
	}
}

fn scrape(user: String, format: String)
{
	let url: String = format!("https://lichess.org/api/user/{}/perf/{}", &user, &format);

	let client = reqwest::blocking::Client::new();
	let resp = client.get(&url)
		.send()
		.unwrap()
		.text()
		.unwrap();
	
	let v: Value = serde_json::from_str(resp.as_str()).unwrap();

	let username = v["user"]["name"]
		.as_str()
		.unwrap();

	let mut chars = format.chars();
	let format = match chars.next()
	{
		None => String::new(),
		Some(f) => f.to_uppercase().collect::<String>() + chars.as_str(),
	};

	println!("{} - {} {}", &username, v["perf"]["glicko"]["rating"], &format);
}